package com.adeli;

import javax.swing.plaf.IconUIResource;
import java.io.*;
import java.net.*;
import java.util.Scanner;

// Java implementation for a client
// Client class
public class Client {
    private String book;

    public Client() {
        book = null;
        try (Scanner scanner = new Scanner(System.in);
             // establish the connection with server port 5056
             Socket socket = new Socket(InetAddress.getByName("localhost"), 5056);
             // obtaining input and out streams
             DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {


            // the following loop performs the exchange of
            // information between client and client handler
            String request;
            String response;
            while (true) {
                response = dataInputStream.readUTF();
                System.out.println(response);
                request = scanner.nextLine();

                if (request.equals("Exit")) {
                    // send request to server
                    dataOutputStream.writeUTF(request);

                    // If client sends exit,close this connection
                    // and then break from the while loop
                    System.out.println("Closing this connection : " + socket);
                    socket.close();
                    System.out.println("Connection closed");
                    break;
                }

                switch (request) {

                    case "B":
                        if (book != null) {
                            request = "";
                            System.out.println("you have borrowed a book before!");
                        }
                        // send request to server
                        dataOutputStream.writeUTF(request);
                        break;

                    case "R":
                        if (book == null) {
                            request = "";
                            System.out.println("Nothing to return!");
                        } else {
                            book = null;
                        }
                        // send request to server
                        dataOutputStream.writeUTF(request);
                        break;

                    default:
                        // send request to server
                        dataOutputStream.writeUTF(request);
                        break;
                }

                // printing date or time as requested by client
                response = dataInputStream.readUTF();
                if (response.contains("present"))
                    book = "present";
                System.out.println(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        Client client = new Client();
    }
}

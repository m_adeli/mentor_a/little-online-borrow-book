package com.adeli;

import java.io.*;
import java.net.*;

// Java implementation of Server side
// It contains two classes : Server and ClientHandler
// Server class is Singleton
public class Server {
    private static Server server;
    private int books;

    private Server() throws IOException {
        // we have 3 books.
        books = 3;

        try (ServerSocket serverSocket = new ServerSocket(5056)) {

            System.out.println("Server started.");

            // running infinite loop for getting
            // client request
            while (true) {
                Socket socket = null;
                try {
                    // socket object to receive incoming client requests
                    socket = serverSocket.accept();
                    System.out.println("A new client is connected : " + socket);

                    // obtaining input and out streams
                    DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

                    System.out.println("Assigning new thread for this client");

                    // create a new thread object
                    Thread thread = new ClientHandler(socket, dataInputStream, dataOutputStream);

                    // Invoking the start() method
                    thread.start();

                } catch (Exception e) {
                    socket.close();
                    e.printStackTrace();
                }
            }

        }

    }

    public static Server getInstance() {
        if (server == null) {
            try {
                server = new Server();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return server;
    }

    public static void main(String[] args) {
        Server srv = Server.getInstance();
    }

    // ClientHandler class
    private class ClientHandler extends Thread {
        final DataInputStream dataInputStream;
        final DataOutputStream dataOutputStream;
        final Socket socket;


        // Constructor
        public ClientHandler(Socket socket, DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
            this.socket = socket;
            this.dataInputStream = dataInputStream;
            this.dataOutputStream = dataOutputStream;
        }

        @Override
        public void run() {
            String request;
            String response;
            while (true) {
                try {

                    // Ask user what he wants
                    dataOutputStream.writeUTF("Do you want to Borrow or Return a Book ?[B | R]..\n" +
                            "Type Exit to terminate connection.");

                    // receive the answer from client
                    request = dataInputStream.readUTF();

                    if (request.equals("Exit")) {
                        System.out.println("Client " + this.socket + " sends exit...");
                        System.out.println("Closing this connection.");
                        this.socket.close();
                        System.out.println("Connection closed");
                        break;
                    }

                    // write on output stream based on the
                    // answer from the client
                    switch (request) {

                        case "B":
                            if (books > 0) {
                                response = "present " + String.valueOf(books);
                                books--;
                            } else {
                                response = "it finished.";
                            }
                            dataOutputStream.writeUTF(response);
                            break;

                        case "R":
                            books++;
                            response = "Thanks.";
                            dataOutputStream.writeUTF(response);
                            break;

                        default:
                            dataOutputStream.writeUTF("Invalid input");
                            break;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                // closing resources
                this.dataInputStream.close();
                this.dataOutputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
